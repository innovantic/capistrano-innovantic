module Capistrano
  module DSL
    module FlywayPaths

      def flyway_migrations_path
        shared_path.join('migrations')
      end

    end
  end
end
