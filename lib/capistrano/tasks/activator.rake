require 'capistrano/dsl/activator_paths'
require 'capistrano/innovantic/helpers'
require 'capistrano/innovantic/i18n'
require 'capistrano/innovantic/env/rvm'
require 'active_support/core_ext/string/inflections'

include Capistrano::Innovantic::Helpers
include Capistrano::DSL::ActivatorPaths

namespace :load do
  task :defaults do
    set :activator_config_file, -> { "conf/#{fetch(:stage)}.conf" }
    set :activator_env_file, -> { ".env.#{fetch(:stage)}" }
    set :activator_log_file, 'logger.xml'
    set :activator_apply_evolutions, false
    set :activator_db_name, 'default'
    set :activator_http_port, 9000
    set :activator_http_address, '127.0.0.1'
    set :activator_assets_compiler, :node
    set :activator_jvm_args, "-J-Xms1024M -J-Xmx1024m -J-server"

    set :foreman_export_format, :upstart
    set :rvm_map_bins, fetch(:rvm_map_bins, []).push('rvmsudo')
    set :nginx_upstream_port, fetch(:activator_http_port)

    set :linked_dirs,   fetch(:linked_dirs, []).push('logs')
    set :linked_files,  fetch(:linked_files, []).push(".env", 'Procfile')
  end
end

namespace :activator do

  ASSETS_COMPILERS = %i(node phantom_js rhino trireme)
  FOREMAN_EXPORT_FORMATS = %i(bluepill inittab launchd runit supervisord systemd upstart)

  def ruby
    ruby_env = fetch(:ruby_env, :rvm)
    case ruby_env
    when :rvm then Capistrano::Innovantic::Env::Rvm
    else
      raise RuntimeError, "Don't know how to handle Ruby environment #{ruby_env.inspect}"
    end.new(self)
  end

  desc 'Copy project configuration'
  task :setup_config do
    on roles :app do
      execute :mkdir, '-pv', activator_config_path
      upload! fetch(:activator_config_file), activator_config_file_path
    end
  end

  desc 'Copy default logger configuration'
  task :setup_logger do
    on roles :app do
      execute :mkdir, '-pv', activator_logs_path
      upload! template('activator/logger_conf.erb'), activator_log_config_file_path
    end
  end

  desc 'Copy foreman Procfile and .env file'
  task :setup_foreman do
    on roles :app do
      execute :mkdir, '-pv', activator_foreman_templates_path
      upload! template('activator/Procfile.erb'), activator_procfile_path
      upload! "#{fetch(:activator_env_file)}", activator_envfile_path
      # TODO make foreman process templates overridable
      upload! activator_local_foreman_templates_path, shared_path, recursive: true
    end
  end

  desc 'Enable process monitoring'
  task :setup_monit do
    on roles :app do
      monit = Capistrano::Innovantic::Monit.new(self)
      monit.monitor_service(:activator)
    end
  end

  desc 'Export application to specified process management'
  task :export_foreman do
    format = fetch(:foreman_export_format)
    on roles :app do
      info "[activator:export_foreman] Exporting application to #{format} format."
      # TODO handle all export formats (only upstart supported for now)
      ruby.sudo_execute! :foreman, :export, :upstart, %Q{/etc/init -a #{fetch(:application)} -u #{host.user} -p #{fetch(:activator_http_port)} -t #{activator_foreman_templates_path}}
    end
  end
  after 'deploy:updated', 'activator:export_foreman'
  after 'deploy:reverted', 'activator:export_foreman'

  desc 'Compile and stage application'
  task :stage do
    compiler = fetch(:activator_assets_compiler)
    abort t(:assets_compiler_does_not_exist,
      compiler: compiler,
      compilers: ASSETS_COMPILERS) unless ASSETS_COMPILERS.include? compiler

    on roles :app do
      within release_path do
        info "[activator:stage] Compiling and staging application using assets compiler : #{compiler}."
        with 'SBT_OPTS' => "-Dsbt.jse.engineType=#{compiler.to_s.camelize}" do
          execute :activator, :clean, :stage
        end
      end
    end
  end
  after  'deploy:updated', 'activator:stage'

  task :restart do
    on roles :app do
      execute :sudo, :monit, :restart, "#{fetch(:application)}"
    end
  end
  after 'deploy:publishing', 'activator:restart'

  task :start do
    on roles :app do
      execute  :sudo, :monit, :start, "#{fetch(:application)}"
    end
  end

  task :stop do
    on roles :app do
      execute :sudo, :monit, :stop, "#{fetch(:application)}"
    end
  end

end

task :setup do
  invoke 'activator:setup_config'
  invoke 'activator:setup_logger'
  invoke 'activator:setup_foreman'
  invoke 'activator:setup_monit'
end
