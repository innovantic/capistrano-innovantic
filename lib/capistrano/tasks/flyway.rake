require 'capistrano/dsl/flyway_paths'

include Capistrano::DSL::FlywayPaths

namespace :load do
  task :defaults do
    set :flyway_user, -> { "#{fetch(:deploy_user)}" }
    set :flyway_password, -> { "#{fetch(:deploy_user)}" }
    set :flyway_url, nil
    set :flyway_migrations, nil

    set :linked_dirs, fetch(:linked_dirs, []).push('migrations')
  end
end

namespace :flyway do

  desc 'copy database migrations'
  task :setup do
    abort t(:flyway_migrations_not_set) if fetch(:flyway_migrations).nil?

    on roles :db do
      execute :mkdir, '-pv', flyway_migrations_path
      upload! fetch(:flyway_migrations), flyway_migrations_path, recursive: true
    end
  end

  task :migrate do
    abort t(:flyway_url_not_set) if fetch(:flyway_url).nil?

    migrations = 'filesystem:' << flyway_migrations_path.to_s
    on roles :db do
      within shared_path do
        execute :flyway, :migrate, %Q{-user=#{fetch(:flyway_user)} -password=#{fetch(:flyway_password)} -url=#{fetch(:flyway_url)} -locations=#{migrations}}
      end
    end
  end
  before 'activator:stage', 'flyway:migrate'

end

task :setup do
  invoke 'flyway:setup'
end
