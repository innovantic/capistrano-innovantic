require 'capistrano/dsl/monit_paths'

include Capistrano::DSL::MonitPaths

namespace :load do
  task :defaults do
    set :monit_path, '/etc/monit'
    set :monit_roles, :app
  end
end

namespace :monit do

  def on_monit_roles(&block)
    on roles(fetch(:monit_roles)) do |host|
      if host.nil?
        instance_exec(nil, nil, &block)
      else
        instance_exec(host, host.user, &block)
      end
    end
  end

  desc 'Print monit processes status'
  task :status do
    on_monit_roles do
      puts capture :sudo, :monit, :status
    end
  end

  desc 'Print monit summary'
  task :sumary do
    on_monit_roles do
      puts capture :sudo, :monit, :summary
    end
  end

  desc 'Reload monit service'
  task :reload do
    on_monit_roles do
      execute :sudo, :service, :monit, :reload
    end
  end

  desc 'Restart monit service'
  task :restart do
    on_monit_roles do
      execute :sudo, :service, :monit, :restart
    end
  end

  desc 'Start all monit services'
  task :start do
    on_monit_roles do
      execute :sudo, :service, :monit, :start, :all
    end
  end

  desc 'Stop all monit services'
  task :stop do
    on_monit_roles do
      execute :sudo, :service, :monit, :stop, :all
    end
  end


end
