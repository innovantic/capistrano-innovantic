module Capistrano
  module Innovantic
    module Db

      class Database
        attr_reader :context

        def initialize(context, options = nil)
          @context = context
          @options = options || {}
        end
      end

    end
  end
end
