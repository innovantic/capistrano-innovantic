module Capistrano
  module DSL
    module PlayPaths

      def play_config_path
        shared_path.join('conf')
      end

      def play_procfile_path
        shared_path.join('Procfile')
      end

      def play_envfile_path
        shared_path.join('.env')
      end

      def play_foreman_templates_path
        shared_path.join('foreman')
      end

      def play_local_foreman_templates_path
        File.expand_path('../../../generators/capistrano/innovantic/templates/foreman', __FILE__)
      end

      def play_pid_path
        File.expand_path("/var/run/#{fetch(:application)}/RUNNING.pid")
      end

      def play_config_file_path
        play_config_path.join("#{fetch(:stage)}.conf")
      end

      def play_logs_path
        shared_path.join('logs')
      end

      def play_log_config_file_path
        play_config_path.join("#{fetch(:play_log_file)}")
      end

      def play_bin_file
        current_path.join("target/universal/stage/bin/#{fetch(:application)}")
      end

    end
  end
end
