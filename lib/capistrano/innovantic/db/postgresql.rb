require 'capistrano/innovantic/db/database'

module Capistrano
  module Innovantic
    module Db

      class PostgreSql < Database
        def dump_to!(user, database, to_path)
          # TODO make dump configurable
          context.execute :pg_dump, "-U #{user} -F c -b -f #{to_path} --encoding utf8 -v #{database}"
        end
      end

    end
  end
end
