module Capistrano
  module DSL
    module MonitPaths

      def monit_site_file(service)
        monit_name = fetch (service.to_s << '_monit_name').to_sym
        "#{fetch(:monit_path)}/conf.d/#{monit_name}.monitrc"
      end

    end
  end
end
