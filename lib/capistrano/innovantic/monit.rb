load File.expand_path("../../tasks/monit.rake", __FILE__)

module Capistrano
  module Innovantic

    class Monit
      attr_reader :context

      def initialize(context, options = nil)
        @context = context
        @options = options || {}
      end

      def monitor_service(service)
        # TODO make monit templates aware of process management (upstart, supervisord...)
        context.info "[monit:monitor_service] Enabling monitoring for service #{service}"
        monit_service_file = monit_site_file(service)
        context.sudo_upload! template("monit/#{service}.monitrc.erb"), monit_service_file
        context.sudo :chown, 'root:root', monit_service_file

        # Reloading monit
        invoke 'monit:reload'
      end

    end
  end
end

