require 'i18n'

en = {
  assets_compiler_does_not_exist: 'Compiler %{compiler} does not exist. Supported ones are %{compilers}',
  flyway_url_not_set: 'Flyway jdbc url is mandatory',
  flyway_migrations_not_set: 'Flyway migrations path is mandatory',
  db_keeping_backups: 'Keeping %{keep_backups} of %{backups} stored backups on %{host}',
  db_no_old_backups: 'No old backups (keeping newest %{keep_backups}) on ${host}'
}

I18n.backend.store_translations(:en, { capistrano: en })

if I18n.respond_to?(:enforce_available_locales=)
  I18n.enforce_available_locales = true
end
