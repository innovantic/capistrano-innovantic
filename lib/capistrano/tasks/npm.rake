require 'capistrano/copy_files'

namespace :load do
  task :defaults do
    set :npm_flags, %w(--silent --no-spin)
    set :npm_roles, :all
    set :copy_files, fetch(:copy_files, []).push('node_modules')
  end
end

namespace :npm do

  desc 'Install local npm packages'
  task :install do
    on roles fetch(:npm_roles) do
      within release_path do
        execute :npm, 'install', fetch(:npm_flags)
      end
    end
  end
  before 'activator:stage', 'npm:install'

end
