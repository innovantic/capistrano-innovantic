namespace :load do
  task :defaults do
    set :gulp_file, nil
    set :gulp_tasks, nil
    set :gulp_flags, '--no-color'
    set :gulp_roles, :all
  end
end

namespace :gulp do

  task :build do
    on roles fetch(:gulp_roles) do
      within fetch(:gulp_target_path, release_path) do
        options = [
          fetch(:gulp_flags)
        ]

        options << "--gulpfile #{fetch(:gulp_file)}" if fetch(:gulp_file)
        options << fetch(:gulp_tasks) if fetch(:gulp_tasks)

        execute :gulp, options
      end
    end
  end
  after 'npm:install', 'gulp:build'

end
