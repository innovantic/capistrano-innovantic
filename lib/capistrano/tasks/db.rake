require 'capistrano/dsl/db_paths'
require 'capistrano/innovantic/db/postgresql'

include Capistrano::DSL::DbPaths

namespace :load do
  task :defaults do
    set :db_backups_dir, 'backups'
    set :db_name, -> { "#{fetch(:deploy_user)}" }
    set :db_user, -> { "#{fetch(:deploy_user)}" }
    set :db_keep_backups, 25
  end
end

namespace :db do

  def db
    db_type = fetch(:db_type, :psql)
    case db_type
    when :psql then Capistrano::Innovantic::Db::PostgreSql
    else
      raise RuntimeError, "Don't know how to handle Database type #{db_type.inspect}"
    end.new(self)
  end

  task :setup do
    on roles :db do
      execute :mkdir, '-pv', db_backup_path
    end
  end

  task :dump do
    on roles :db do
      database = fetch(:db_name)
      dump_file_name = "#{Time.now.utc.strftime('%Y%m%d%H%M%S')}.backup"
      dump_to = db_backup_path.join(dump_file_name)
      info "[db:dump] Dumping database #{database} to #{dump_to}."
      db.dump_to!(fetch(:db_user), database, dump_to)
    end
  end
  before 'deploy:starting', 'db:dump'

  task :sync do
    on roles :db do
      # TODO: implement db sync from dump
    end
  end

  task :cleanup do
    on roles :db do |host|
      backups = capture(:ls, '-xtr', db_backup_path).split
      if backups.count >= fetch(:db_keep_backups)
        info t(:db_keeping_backups, host: host.to_s, keep_backups: fetch(:db_keep_backups), backups: backups.count)
        files = (backups - backups.last(fetch(:db_keep_backups)))
        if files.any?
          files_str = files.map do |backup|
            db_backup_path.join(backup)
          end.join(" ")
          execute :rm, '-f', files_str
        else
          info t(:db_no_old_backups, host: host.to_s, keep_backups: fetch(:db_keep_backups))
        end
      end
    end
  end
  after 'deploy:finishing', 'db:cleanup'
end

task :setup do
  invoke 'db:setup'
end
