require 'capistrano/rvm'
require 'capistrano/innovantic/env/ruby'

module Capistrano
  module Innovantic
    module Env

      class Rvm < Ruby
        def sudo_execute!(*args)
          c = SSHKit::Command.new(:rvmsudo, *args, in: "#{fetch(:release_path)}")
          context.execute c.to_command
        end
      end

    end
  end
end
