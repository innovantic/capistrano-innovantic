module Capistrano
  module DSL
    module ActivatorPaths

      def activator_config_path
        shared_path.join('conf')
      end

      def activator_procfile_path
        shared_path.join('Procfile')
      end

      def activator_envfile_path
        shared_path.join('.env')
      end

      def activator_foreman_templates_path
        shared_path.join('foreman')
      end

      def activator_local_foreman_templates_path
        File.expand_path('../../../generators/capistrano/innovantic/templates/foreman', __FILE__)
      end

      def activator_pid_path
        File.expand_path("/var/run/#{fetch(:application)}/RUNNING.pid")
      end

      def activator_config_file_path
        activator_config_path.join("#{fetch(:stage)}.conf")
      end

      def activator_logs_path
        shared_path.join('logs')
      end

      def activator_log_config_file_path
        activator_config_path.join("#{fetch(:activator_log_file)}")
      end

      def activator_bin_file
        current_path.join("target/universal/stage/bin/#{fetch(:application)}")
      end

    end
  end
end
