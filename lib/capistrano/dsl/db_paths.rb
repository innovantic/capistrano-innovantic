module Capistrano
  module DSL
    module DbPaths

      def db_backup_path
        shared_path.join("#{fetch(:db_backups_dir)}")
      end

    end
  end
end
