# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano/innovantic/version'

Gem::Specification.new do |spec|
  spec.name          = "capistrano-innovantic"
  spec.version       = Capistrano::Innovantic::VERSION
  spec.authors       = ["Jean-Loïc Hervo"]
  spec.email         = ["jlhervo@innovantic.fr"]
  spec.summary       = %q{Bunch of Capistrano 3 tasks to automate every day tasks.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'capistrano', '~> 3.2'
  spec.add_dependency 'capistrano-rvm'
  spec.add_dependency 'capistrano-copy-files'
  spec.add_dependency 'activesupport', '~> 4.1'

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~> 10.0"
end
