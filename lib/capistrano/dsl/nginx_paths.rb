module Capistrano
  module DSL
    module NginxPaths

      def nginx_sites_available_file
        "#{fetch(:nginx_path)}/sites-available/#{fetch(:application)}"
      end

      def nginx_sites_enabled_file
        "#{fetch(:nginx_path)}/sites-enabled/#{fetch(:application)}"
      end

      def nginx_default_ssl_cert_file_name
        "#{fetch(:application)}.crt"
      end

      def nginx_default_ssl_cert_key_file_name
        "#{fetch(:application)}.key"
      end

      def nginx_ssl_cert_file
        "/etc/ssl/certs/#{fetch(:nginx_ssl_cert)}"
      end

      def nginx_ssl_cert_key_file
        "/etc/ssl/private/#{fetch(:nginx_ssl_cert_key)}"
      end

    end
  end
end
