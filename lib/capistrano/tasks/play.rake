require 'capistrano/dsl/play_paths'
require 'capistrano/innovantic/helpers'
require 'capistrano/innovantic/i18n'
require 'capistrano/innovantic/env/rvm'
require 'active_support/core_ext/string/inflections'

include Capistrano::Innovantic::Helpers
include Capistrano::DSL::PlayPaths

namespace :load do
  task :defaults do
    set :play_config_file, -> { "conf/#{fetch(:stage)}.conf" }
    set :play_env_file, -> { ".env.#{fetch(:stage)}" }
    set :play_log_file, 'logger.xml'
    set :play_apply_evolutions, false
    set :play_db_name, 'default'
    set :play_http_port, 9000
    set :play_http_address, '127.0.0.1'

    set :foreman_export_format, :upstart
    set :rvm_map_bins, fetch(:rvm_map_bins, []).push('rvmsudo')
    set :nginx_upstream_port, fetch(:play_http_port)

    set :linked_dirs,   fetch(:linked_dirs, []).push('logs')
    set :linked_files,  fetch(:linked_files, []).push(".env", 'Procfile')
  end
end

namespace :play do

  FOREMAN_EXPORT_FORMATS = %i(bluepill inittab launchd runit supervisord systemd upstart)

  def ruby
    ruby_env = fetch(:ruby_env, :rvm)
    case ruby_env
    when :rvm then Capistrano::Innovantic::Env::Rvm
    else
      raise RuntimeError, "Don't know how to handle Ruby environment #{ruby_env.inspect}"
    end.new(self)
  end

  desc 'Copy project configuration'
  task :setup_config do
    on roles :app do
      execute :mkdir, '-pv', play_config_path
      upload! fetch(:play_config_file), play_config_file_path
    end
  end

  desc 'Copy default logger configuration'
  task :setup_logger do
    on roles :app do
      execute :mkdir, '-pv', play_logs_path
      upload! template('play/logger_conf.erb'), play_log_config_file_path
    end
  end

  desc 'Copy foreman Procfile and .env file'
  task :setup_foreman do
    on roles :app do
      execute :mkdir, '-pv', play_foreman_templates_path
      upload! template('play/Procfile.erb'), play_procfile_path
      upload! "#{fetch(:play_env_file)}", play_envfile_path
      # TODO make foreman process templates overridable
      upload! play_local_foreman_templates_path, shared_path, recursive: true
    end
  end

  desc 'Enable process monitoring'
  task :setup_monit do
    on roles :app do
      monit = Capistrano::Innovantic::Monit.new(self)
      monit.monitor_service(:play)
    end
  end

  desc 'Export application to specified process management'
  task :export_foreman do
    format = fetch(:foreman_export_format)
    on roles :app do
      info "[play:export_foreman] Exporting application to #{format} format."
      # TODO handle all export formats (only upstart supported for now)
      ruby.sudo_execute! :foreman, :export, format, %Q{/etc/init -a #{fetch(:application)} -u #{host.user} -p #{fetch(:play_http_port)} -t #{play_foreman_templates_path}}
    end
  end
  after 'deploy:updated', 'play:export_foreman'
  after 'deploy:reverted', 'play:export_foreman'

  desc 'Compile and stage application'
  task :stage do
    on roles :app do
      within release_path do
        info "[play:stage] Compiling and staging application."
          execute :play, :clean, :stage
      end
    end
  end
  after  'deploy:updated', 'play:stage'

  task :restart do
    on roles :app do
      execute :sudo, :monit, :restart, "#{fetch(:application)}"
    end
  end
  after 'deploy:publishing', 'play:restart'

  task :start do
    on roles :app do
      execute  :sudo, :monit, :start, "#{fetch(:application)}"
    end
  end

  task :stop do
    on roles :app do
      execute :sudo, :monit, :stop, "#{fetch(:application)}"
    end
  end

end

task :setup do
  invoke 'play:setup_config'
  invoke 'play:setup_logger'
  invoke 'play:setup_foreman'
  invoke 'play:setup_monit'
end
